var http = require('http');
var fs = require('fs');
var url = require('url');

//端口
var port = 80;
//默认首页
var home = "/home.html";

// 创建服务器
 var server = http.createServer( function (request, response) {  
   // 解析请求，包括文件名
   var pathname = url.parse(request.url).pathname;
   
   // 输出请求的文件名
   console.log("Request for " + pathname + " received.");
   if(pathname == "/"){
	   pathname = home;
   }   
   // 从文件系统中读取请求的文件内容
   fs.readFile(pathname.substr(1), function (err, data) {
      if (err) {
         console.log(err);
         // HTTP 状态码: 404 : NOT FOUND
         response.writeHead(404, {'Content-Type': 'text/html'});
      }else{	         
         // HTTP 状态码: 200 : OK
		 if(pathname.indexOf(".css") >0){ //css
			 response.writeHead(200, {'Content-Type': 'text/css'});
		 }else if(pathname.indexOf(".js") >0){ //js
			 response.writeHead(200, {'Content-Type': 'text/js'});
		 }else{ //其它, html
			response.writeHead(200, {'Content-Type': 'text/html'});
		 }
         //响应文件内容
         response.write(data.toString());		
      }
      //发送响应数据
      response.end();
   });   
});
//开始监听端口
server.listen(port);

// 控制台会输出以下信息
console.log('Server running at port:' +port);