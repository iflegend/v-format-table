/*
 * @auther Vanch
 * 子组件合并到一个文件，含格式化表格和选项式表格
 */
 (function() {
	'use strict';
	//表格操作组件
	window.CommonOperation = React.createClass({
		handleClick: function(){
			var data = this.props.data;
			var op = this.props.op;
			op.callback(op.opid, data);
		},
		render: function() {
			var data = this.props.data;			    
			var op = this.props.op;
			return(
			  <a onClick={this.handleClick} opid={op.opid}><i className="fa fa-fw" title="edit"/>{op.name}</a>
			)
		}
	});
	//表格操作组件组合
	window.CommonTDOperations = React.createClass({
		render: function() {
			var data = this.props.data;
			var list = this.props.ops.map(function (key, index) {
			  return (			        		        
				  <CommonOperation op={key} key={index} data={data}/>
			  );
			});
			
			return(	
					<td className="edit-td">{list}</td>
			);
		}
	});
	//表格行TR组件
	window.CommonTR = React.createClass({
		render: function() {
			var data = this.props.data;		
			var _this=this;
			var list = this.props.tds.map(function (key, index) {
				var cont = "";
				var has_td = false;
				
				//use one in two.
				try{
					cont = data[key['col']];
				}catch(e){}
				try{						
					cont = key['format'](data,_this.props.dataIndex,index);		
				}catch(e){}
				
				//if 'has_td', return 'cont' directly, else return with 'td'.
				try{
					has_td = key['has_td'];
				}catch(e){}
				if(has_td){
					return (cont);
				}else{
					return (<td key={index}>{cont}</td>);
				}			      
		   });
			return(
			  <tr>{list}</tr>
			)
		}
	});
	//表格表头thead组件
	window.CommonTHead = React.createClass({
		render: function() {
			var list = this.props.ths.map(function (key, index) {
				var cont = "";		
				var has_th = false;
				//use one in two.
				try{
					cont = key['colName'];
				}catch(e){}
				try{						
					cont = key['format'](index);
				}catch(e){}
				//if 'has_td', return 'cont' directly, else return with 'td'.
				try{
					has_th = key['has_th'];
				}catch(e){}
				if(has_th){
					return (cont);
				}else{
					return (<th key={index}>{cont}</th>);
				}
			});
			return(
			  <thead><tr>{list}</tr></thead>
			)
		}
	});
	//表格表体tbody组件
	window.CommonTBody = React.createClass({
		render: function() {
			var tds = this.props.tds;
			var list = this.props.data.map(function (key, index) {
			  return (
				<CommonTR key={index} dataIndex={index} tds={tds} data={key}/>
			  );
			});
			return(
			  <tbody>{list}</tbody>
			)
		}
	});
	//表格式选项组件
	window.OptionTable = React.createClass({
		getDefaultProps:function(){
			return {
				isRadio:false
			};
		},
		getInitialState:function(){
			var options = this.props.options?this.props.options:[];
			var data = [];
			var tds = [];
			var self = this;
			options.map(function(items, i){
				data[i] = {};
				items.map(function(item,j){
					data[i][j] = item;
					tds[j] = {};
					tds[j]['col'] = j;
					tds[j]['format'] = self.formatOption;
				});			
			});
			return {
				isRadio:this.props.isRadio,
				data:data,
				tds:tds
			};
		},
		componentWillReceiveProps:function(nextProps){
			if(nextProps.isRadio){
				this.state.isRadio = nextProps.isRadio;
			}
			if(nextProps.options){
				var options = nextProps.options?nextProps.options:[];
				var data = [];
				var tds = [];
				var self = this;
				options.map(function(items, i){
					data[i] = {};
					items.map(function(item,j){
		//				alert(item +", " +i +", " +j);//test!!!
						data[i][j] = item;
						tds[j] = {};
						tds[j]['col'] = j;
						tds[j]['format'] = self.formatOption;
					});			
				});
	//			alert(JSON.stringify(data));
				this.setState({data:data, tds:tds});
			}
			
		},
		onChange:function(e){
	//		alert(e.target.checked);
			if(this.props.callback){
				this.props.callback(e.target.value, e.target.checked);
			}
		},
		formatOption:function(data,checkIndex, col){
			if(!data[col]){
				return "";
			}
			if(this.state.isRadio == true){
				return (<span ><input type="radio" name="myRadio" defaultValue={data[col]['value']} defaultChecked={data[col]['checked']} onChange={this.onChange}></input>{data[col]['label']}</span>);
			}else
			{
				return (<span ><input type="checkbox" name="myCheck" defaultValue={data[col]['value']} defaultChecked={data[col]['checked']} onChange={this.onChange}></input>{data[col]['label']}</span>);
			}
		},
	  render: function() {    
		return (
		  <table className="table-condensed" style={{marginTop:"-5px"}}>
			<CommonTBody data={this.state.data} tds={this.state.tds}/>
		  </table>
		);
	  }
	});
	//格式化表格组件
	window.FormatTable = React.createClass({
	getInitialState:function(){
		return {data:this.props.data,
			ths:this.props.ths,
			tds:this.props.tds};
	},
	componentWillReceiveProps:function(nextProps){
		if(nextProps.data){
			this.setState({data:nextProps.data});
		}
	},
  render: function() {    
    return (
      <table className="table table-striped table-bordered table-hover table-condensed" id="dataTables-example">                        
        <CommonTHead ths={this.state.ths}/>
        <CommonTBody data={this.state.data} tds={this.state.tds}/>
      </table>
    );
  }
});
})();