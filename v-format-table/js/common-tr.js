/*
 * @auther Vanch
 * 表格行tr组件
 */
 (function() {
	'use strict';
	window.CommonTR = React.createClass({
			render: function() {
				var data = this.props.data;		
				var _this=this;
				var list = this.props.tds.map(function (key, index) {
					var cont = "";
					var has_td = false;
					
					//use one in two.
					try{
						cont = data[key['col']];
					}catch(e){}
					try{						
						cont = key['format'](data,_this.props.dataIndex,index);		
					}catch(e){}
					
					//if 'has_td', return 'cont' directly, else return with 'td'.
					try{
						has_td = key['has_td'];
					}catch(e){}
					if(has_td){
						return (cont);
					}else{
						return (<td key={index}>{cont}</td>);
					}			      
			   });
			    return(
			      <tr>{list}</tr>
			    )
			}
		});

	//TODO: 组件自测代码test!!!
})();