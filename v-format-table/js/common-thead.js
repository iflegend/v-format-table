/*
 * @auther Vanch
 * 表格表头thead组件
 */
 (function() {
	'use strict';
	window.CommonTHead = React.createClass({
			render: function() {
				var list = this.props.ths.map(function (key, index) {
					var cont = "";		
					var has_th = false;
					//use one in two.
					try{
						cont = key['colName'];
					}catch(e){}
					try{						
						cont = key['format'](index);
					}catch(e){}
					//if 'has_td', return 'cont' directly, else return with 'td'.
					try{
						has_th = key['has_th'];
					}catch(e){}
					if(has_th){
						return (cont);
					}else{
						return (<th key={index}>{cont}</th>);
					}
			    });
			    return(
			      <thead><tr>{list}</tr></thead>
			    )
			}
		});

	//TODO: 组件自测代码test!!!
//  var ths = ["仓库ID", "仓库名称", "上级仓库", "所属门店", "创建时间", "操作"];
//	ReactDOM.render(		 
//  <CommonTHead ths={ths}/>,
//  document.getElementById('mycontainer')
//);
})();