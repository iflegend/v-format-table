/**
 * @author Vanch
 * tab组件
 */
(function() {
	'use strict';
	window.CommonTabs = React.createClass({
		getInitialState:function(){
			return {	
				currentIndex:0
			};
		},
		willMountComponent:function(){
			this.setState({currentIndex:this.props.currentIndex});
		},
		handleClick: function(e){
			var index = e.target.id;
			try{
				this.setState({currentIndex:index});
				this.props.callback(index);	
			}catch(e){
				
			}			
		},
		render: function() {
			var self = this;
			var list = this.props.data.map(function(key, index){
				if(self.state.currentIndex == index){
					return (
					<li className="active" key={index}><a href={key.href} id={index} onClick={self.handleClick}>{key.name}</a></li>
					);
				}else
				return (
					<li key={index}><a href={key.href} id={index} onClick={self.handleClick}>{key.name}</a></li>
				);
			});
		    return(
		    	<ul className="nav nav-tabs">
		    		{list}
		    	</ul>
		    )
		}
	});
})();