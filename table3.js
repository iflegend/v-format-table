/**
 * @author Vanch
 */
(function() {
	'use strict';
	window.Table3 = React.createClass({
	getInitialState:function(){
		var fileOptions = [[{label:"无", value:0}]];
		var tagOptions =  [[{label:"无", value:0}]];
		return {
			fileOptions:fileOptions,
			tagOptions:tagOptions
			};
	},
	componentWillMount:function(){
		//TODO AJAX, get data before mount
		//...
		//test!!!
		var fileOptions = [		    		    
		    [{label:"文件1", value:1}, {label:"文件2", value:2}, {label:"文件3", value:3},{label:"文件4", value:4},{label:"文件5", value:5}],
		    [{label:"文件6", value:6}, {label:"文件7", value:7}, {label:"文件8", value:8},{label:"文件9", value:9},{label:"文件10", value:10}],
		    [{label:"文件11", value:11}, {label:"文件12", value:12}, {label:"文件13", value:13}]	
		    ];
		var tagOptions = [	    		    
		    [{label:"标签1", value:1}, {label:"标签2", value:2}, {label:"标签3", value:3}],
		    [{label:"标签4", value:4},{label:"标签5", value:5}],
		    [{label:"标签6", value:6}, {label:"标签7", value:7}], 
		    [{label:"标签8", value:8},{label:"标签9", value:9},{label:"标签10", value:10}],
		    [{label:"标签11", value:11}, {label:"标签12", value:12}, {label:"标签13", value:13}],
		    [{label:"标签14", value:14}]
		    ];
		this.setState({fileOptions:fileOptions, tagOptions:tagOptions});
	},
	fileSelected:function(value, checked){
		if(checked){
			alert(value+"被选中");
		}else{
			alert(value+"被取消选中");
		}
	},
	tagSelected:function(value, checked){
		if(checked){
			alert(value+"被选中");
		}else{
			alert(value+"被取消选中");
		}
	},
  render: function() {
    return (
    	<div>
    	
		<div className="row">
				<label className="col-md-1">单选文件</label>
				<div className="col-md-6">
				<OptionTable options={this.state.fileOptions} isRadio={true}
								callback={this.fileSelected}/>
				</div>
		</div>
		<br/><br/>
		<div className="row">
				<label className="col-md-1">多选标签</label>
				<div className="col-md-6">
				<OptionTable options={this.state.tagOptions} isRadio={false}
								callback={this.tagSelected}/>
				</div>
		</div>
		
		</div>
    );
  }
});

})();