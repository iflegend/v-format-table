/**
 * @author Vanch
 */
(function() {
	'use strict';
	var tabContent;
	window.Check = React.createClass({
	getInitialState:function(){
		var count =3;
		return {
			tabIndex:0
			};
	},
	componentDidMount:function(){
		ReactDOM.render(
		    <Table1/>,
		    document.getElementById('my-tab-content')
		);
	},
	tabChanged:function(index){
		if(this.state.tabIndex == index){
			//do nothing
		}else{
			var elem = document.getElementById('my-tab-content');
			if(tabContent){				
				ReactDOM.unmountComponentAtNode(elem);
			}
			if(index == 0){
				ReactDOM.render(
				    <Table1/>,
				    document.getElementById('my-tab-content')
				);
			}else if(index == 1){				
				ReactDOM.render(
				    <Table2/>,
				    document.getElementById('my-tab-content')
				);
			}else if(index == 2){
				ReactDOM.render(
				    <Table3/>,
				    document.getElementById('my-tab-content')
				);
			}else{
				ReactDOM.render(
				    <Table1/>,
				    document.getElementById('my-tab-content')
				);
			}
			this.state.tabIndex = index;
		}		
	},
  render: function() {
    return (
    	<div>    		
			<h1>V-Format-Table</h1>
			<br/>
			<CommonTabs data={[{name:"Table1"}, {name:"Table2"}, {name:"Table3"}]} callback={this.tabChanged}></CommonTabs>
			<br/>
			<div id="my-tab-content" className="tab-content">
        	
         	</div>
        </div>
    );
  }
});

ReactDOM.render(
    <Check/>,
    document.getElementById('container')
);

})(Zepto);